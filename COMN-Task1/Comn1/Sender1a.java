/**
 * Created by s1443483 on 05/02/17.
 */

import java.io.*;
import java.net.*;

public class Sender1a {

    public static void main(String args[]) throws Exception {

        DatagramSocket senderSocket = new DatagramSocket(5432);    // Socket initialisation

        byte sendData[] = new byte[1027];
        int s = 0;
        byte flag = 0;
        FileInputStream file = new FileInputStream(new File(args[2]));
        File image = new File(args[2]);
        double fileSize = Math.ceil((image.length()/1024) + 1);    // The number of packets to be sent
        InetAddress IPAddress=InetAddress.getByName(args[0]);
        short seqNum = 0;   //Sequence Number
        int counter = 0;
        // Loop that is iterated as long as the file has not been read completely
        while((s = file.read(sendData,3,sendData.length-3)) != -1) {
            counter++;
            // Converting the short number to 2 bytes
            sendData[0] = (byte)(seqNum & 0xff);
            sendData[1] = (byte)((seqNum >> 8) & 0xff);
            seqNum++;
            // If it's the last iteration, i.e. end of file, flag becomes 1
            if(counter == fileSize){
                flag = 1;
            }
            sendData[2] = flag;    // Flag to be sent so that the Receiver knows when to close the socket
            byte sendData2[] = new byte[s+3];
            // New array with size equaling the number of bytes read plus 3 (sequence number and flag)
            for(int i = 0; i < sendData2.length; i++){
                sendData2[i] = sendData[i];
            }
            Thread.sleep(10);
            // Send the packet
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData2, s+3, IPAddress, Integer.parseInt(args[1]));
            senderSocket.send(sendPacket);

        }
        senderSocket.close();    // Close the socket

    }
}
