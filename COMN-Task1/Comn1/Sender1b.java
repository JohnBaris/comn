/**
 * Created by Yannis on 11/02/2017.
 */

import java.io.*;
import java.net.*;

public class Sender1b {
    public static void main(String args[]) throws Exception {

        DatagramSocket senderSocket = new DatagramSocket(5432);    // Socket initialisation

        byte sendData[] = new byte[1027];
        byte ACK[] = new byte[2];    // The acknowledgment byte array
        ACK[0] = 1;    // Initialise the first byte
        int s = 0;
        byte flag = 0;
        FileInputStream file = new FileInputStream(new File(args[2]));
        File image = new File(args[2]);
        double fileSize = Math.ceil((image.length()/1024) + 1);    // The number of packets to be sent
        InetAddress IPAddress=InetAddress.getByName(args[0]);
        short seqNum = 0;   //Sequence Number
        int counter = 0;
        int lastPacketRetrans = 0;
        int retransmissions=0;    // Number of retransmissions
        long startTime = System.currentTimeMillis();    // Begin counting the runtime
        double ack = 1;    // Initialise the acknowledgement counter
        // Loop that is iterated as long as the file has not been read completely
        while((s = file.read(sendData,3,sendData.length-3)) != -1) {
            // The sequence number interchanges between 0 and 1
            if(counter % 2 == 0) {
                seqNum = 0;
            }
            else{
                seqNum = 1;
            }
            // Converting the short number to 2 bytes
            sendData[0] = (byte)(seqNum & 0xff);
            sendData[1] = (byte)((seqNum >> 8) & 0xff);
            // If it's the last iteration, i.e. end of file, flag becomes 1
            if((counter + 1) == fileSize){
                flag = 1;
            }
            sendData[2] = flag;    // Flag to be sent so that the Receiver knows when to close the socket
            byte sendData2[] = new byte[s+3];
            // New array with size equaling the number of bytes read plus 3 (sequence number and flag)
            for(int i = 0; i < sendData2.length; i++){
                sendData2[i] = sendData[i];
            }
            // Send the packet
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData2, s+3, IPAddress, Integer.parseInt(args[1]));
            senderSocket.send(sendPacket);
            senderSocket.setSoTimeout(Integer.parseInt(args[3]));

            // Wait for the acknowledgement
            DatagramPacket ACKPacket = new DatagramPacket(ACK,2);
            // Try to receive acknowledgment and resend the packet until the received acknowledgement is the expected one
            while(ACK[0]!=sendData[0]) {
                try {
                    senderSocket.receive(ACKPacket);

                } catch (SocketTimeoutException e) {    // Timeout
                    senderSocket.send(sendPacket);
                    retransmissions++;
                    // Condition for dealing with the case where timeout happens before receiving the acknowledgement for the last packet, which leads to Receiver finishing and Sender being stuck in the loop
                    if(ack == fileSize) {
                        lastPacketRetrans++;
                    }
                    if(lastPacketRetrans==20){
                        break;
                    }
                    senderSocket.setSoTimeout(Integer.parseInt(args[3]));
                }
            }
            ack++;
            counter++;
        }
        senderSocket.close();    // Close the socket

        long endTime = System.currentTimeMillis();    // Stop counting runtime
        double totalTime = (double)(endTime - startTime)/1000.000;    // Total program execution/transmission time
        double throughput = (image.length()/1000.000)/totalTime;    // Throughput
        System.out.println(totalTime + " Total transmission time");
        System.out.println(throughput+ " Throughput");
        System.out.println(retransmissions + " Retransmissions");

    }
}
