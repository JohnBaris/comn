import java.io.FileOutputStream;
import java.net.*;

/**
 * Created by s1443483 on 05/02/17.
 */
public class Receiver1a {
    public static void main(String args[]) throws Exception {
        DatagramSocket receiverSocket = new DatagramSocket(Integer.parseInt(args[0]));    // Socket initialisation
        byte[] receiveData = new byte[1027];

        FileOutputStream fos = new FileOutputStream(args[1]);    // File to be written
        int counter = 0;
        // Eternal loop until break happens
        while (true) {
            counter++;
            // Receive the packet
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            receiverSocket.receive(receivePacket);
            receiveData = receivePacket.getData();
            // New byte array which has as size the actual length of the packet received
            byte[] changedData = new byte[receivePacket.getLength()-3];
            for(int i = 3;i<receivePacket.getLength();i++) {
                changedData[i-3] = receiveData[i];
            }
            fos.write(changedData);    // Write the byte array to the file
            // If the flag of the packet that was sent is 1 then break from the loop
            if(receiveData[2] == 1){
                break;
            }
        }
        fos.close();    // Close the file
        receiverSocket.close();    // Close the socket
    }
}
