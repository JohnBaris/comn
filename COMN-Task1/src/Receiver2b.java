/**
 * Matriculation Number:s1443483
 * Name: Ioannis Baris
 */

import java.io.FileOutputStream;
import java.net.*;
import java.util.*;

public class Receiver2b {

    public static void main(String args[]) throws Exception {
        DatagramSocket receiverSocket = new DatagramSocket(Integer.parseInt(args[0]));    // Socket initialisation
        byte[] receiveData = new byte[1027];

        FileOutputStream fos = new FileOutputStream(args[1]);    // File to be written
        short expectedAck = 1;                                   // The expected acknowledgement/sequence number
        short defaultAck = 0;                                    // The default/actual acknowledgement/sequence number
        int windowSize = Integer.parseInt(args[2]);              // Window size
        short receivedSeqNum;                                    // The sequence number of the received packet
        boolean newPacket;
        // ArrayList where the sequence numbers of the received packets are saved
        ArrayList<Short> receivedSeqNums = new ArrayList<Short>();
        // HashMap where the out-of-order, but within window, packets are stored
        Map<Short,byte[]> bufferedPacks = new HashMap<Short,byte[]>();

        // Eternal loop until break happens
        while (true) {

            // Receive the packet
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            // If the socket stops receiving any packets for some time then we break from the loop and the program ends
            // i.e. Grace Period
            try {
                receiverSocket.receive(receivePacket);
            }
            catch(SocketTimeoutException e){
                break;
            }

            // Converting the short number to 2 bytes
            byte[] ACK = new byte[2];
            byte[] expACK = new byte[2];
            ACK[1] = (byte)(defaultAck & 0xff);
            ACK[0] = (byte)((defaultAck >> 8) & 0xff);
            expACK[1] = (byte)(expectedAck & 0xff);
            expACK[0] = (byte)((expectedAck >> 8) & 0xff);

            // New byte array which has as size the actual length of the packet received
            byte[] changedData;
            receiveData = receivePacket.getData();
            changedData = Arrays.copyOfRange(receiveData, 3, receivePacket.getLength());

            ACK[1] = receiveData[1];
            ACK[0] = receiveData[0];
            receivedSeqNum = (short) ((ACK[1] & 0xFF) | ACK[0] << 8);

            // Checks if the sequence number of the received packet is within the window
            if(receivedSeqNum>=expectedAck && receivedSeqNum<=expectedAck + windowSize - 1) {

                // Add the sequence number to the list if it hasn't been added before
                if (!receivedSeqNums.contains(receivedSeqNum)) {
                    receivedSeqNums.add(receivedSeqNum);
                    newPacket = true;
                }
                else{
                    newPacket = false;
                }

                // Check that the received packet is the expected one
                if(receivedSeqNum == expectedAck) {

                    if (newPacket == true) {

                        fos.write(changedData);    // Write the byte array to the file
                        expectedAck++;
                        // Write all the packets within the window that were buffered before getting the expected packet
                        while(bufferedPacks.get(expectedAck) !=null){
                            fos.write(bufferedPacks.get(expectedAck));    // Write the byte array to the file
                            expectedAck++;
                        }
                        defaultAck = expectedAck;

                    }
                    else{
                        expectedAck++;
                    }
                }
                else{
                    // If the received packet is not the expected one, then store it to the buffer if it isn't there
                    if(!bufferedPacks.containsKey(receivedSeqNum)) {
                        bufferedPacks.put(receivedSeqNum, changedData);
                    }

                }
                // Send the acknowledgement
                DatagramPacket ACKPacket = new DatagramPacket(ACK, 2, receivePacket.getAddress(), receivePacket.getPort());
                receiverSocket.send(ACKPacket);
            }
            //
            else if(receivedSeqNum>=receivedSeqNum-windowSize && receivedSeqNum<=expectedAck-1){

                // Send the acknowledgement
                DatagramPacket ACKPacket = new DatagramPacket(ACK, 2, receivePacket.getAddress(), receivePacket.getPort());
                receiverSocket.send(ACKPacket);

            }

            receiverSocket.setSoTimeout(1500);
        }
        fos.close();    // Close the file
        receiverSocket.close();    // Close the socket
    }
}
