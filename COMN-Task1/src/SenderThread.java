/**
 * Matriculation Number:s1443483
 * Name: Ioannis Baris
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Timer;
import java.util.TimerTask;

public class SenderThread implements  Runnable {

    private DatagramPacket packet = null;   // Packet to be sent
    private DatagramSocket socket = null;   // Socket of the sender
    private Timer time = new Timer();       // Create a time instance of the Timer class
    private Sender2b sender;                // Create an instance of the Sender2b class
    private short seq;                      // The sequence number of the packet to be sent
    int timeout;                            // Timeout value
    int window;

    public SenderThread(Short seq,Sender2b main,DatagramSocket socket, DatagramPacket packet, Integer timeout, Integer window){
        this.socket = socket;
        this.packet = packet;
        this.sender = main;
        this.seq = seq;
        this.timeout = timeout;
        this.window = window;
    }

    @Override
    public void run() {

        // Schedules a task which retransmits the packet after "timeout" ms
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    // If the acknowledgement for the packet has been received, then cancel the timer (scheduled task)
                    if (sender.getAck().contains(seq)) {
                        time.cancel();
                        time.purge();
                    }
                    socket.send(packet);       // Send the packet
                    if(window<=64) {
                        Thread.sleep(2 * window);
                    }
                } catch (IOException e) {
//                           e.printStackTrace();
                    time.cancel();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, timeout, timeout);
    }
}
