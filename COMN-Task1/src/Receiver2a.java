/**
 * Matriculation Number:s1443483
 * Name: Ioannis Baris
 */

import java.io.FileOutputStream;
import java.net.*;
import java.util.Arrays;

public class Receiver2a {

    public static void main(String args[]) throws Exception {
        DatagramSocket receiverSocket = new DatagramSocket(Integer.parseInt(args[0]));    // Socket initialisation
        byte[] receiveData = new byte[1027];

        FileOutputStream fos = new FileOutputStream(args[1]);    // File to be written
        short expectedAck = 1;                                   // The expected acknowledgement/sequence number
        short defaultAck = 0;                                    // The default/actual acknowledgement/sequence number
        boolean correctPacket;

        // Eternal loop until break happens
        while (true) {

            // Receive the packet
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            receiverSocket.receive(receivePacket);

            // Converting the short number to 2 bytes
            byte[] ACK = new byte[2];
            byte[] expACK = new byte[2];
            ACK[1] = (byte)(defaultAck & 0xff);
            ACK[0] = (byte)((defaultAck >> 8) & 0xff);
            expACK[1] = (byte)(expectedAck & 0xff);
            expACK[0] = (byte)((expectedAck >> 8) & 0xff);

            // New byte array which has as size the actual length of the packet received
            byte[] changedData;
            receiveData = receivePacket.getData();;
            changedData = Arrays.copyOfRange(receiveData, 3, receivePacket.getLength());

            // Check that the sequence number of the received packet is the expected one
            if (expACK[0] == receiveData[0] && expACK[1] == receiveData[1]) {

                fos.write(changedData);     // Write the byte array to the file
                defaultAck = expectedAck;   // Default acknowledgement takes the value of the expected one
                expectedAck++;
                correctPacket = true;

            }
            else{

                correctPacket = false;

            }

            // Send the acknowledgement of the last correct packet received
            ACK[1] = (byte)(defaultAck & 0xff);
            ACK[0] = (byte)((defaultAck >> 8) & 0xff);
            DatagramPacket ACKPacket = new DatagramPacket(ACK, 2, receivePacket.getAddress(), receivePacket.getPort());
            receiverSocket.send(ACKPacket);


            // If the flag of the packet that was sent is 1 then break from the loop
            if(receiveData[2] == 1 && correctPacket==true){
                break;
            }
        }

        fos.close();    // Close the file
        receiverSocket.close();    // Close the socket
    }
}
