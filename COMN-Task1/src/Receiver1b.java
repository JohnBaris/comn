/**
 * Created by Yannis on 11/02/2017.
 */

import java.io.FileOutputStream;
import java.net.*;

public class Receiver1b {

    public static void main(String args[]) throws Exception {
        DatagramSocket receiverSocket = new DatagramSocket(Integer.parseInt(args[0]));    // Socket initialisation
        byte[] receiveData = new byte[1027];
        byte[] sequence = new byte[1000];    // Sequence number byte array

        FileOutputStream fos = new FileOutputStream(args[1]);    // File to be written
        int duple = 0;    // Variable that becomes 1 when the packet is duplicate
        int counter = 0;
        int dupleCount = 0;    // Variable for incrementing the sequence number byte array index
        short ack = 0;
        // Eternal loop until break happens
        while (true) {
            // Receive the packet
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            receiverSocket.receive(receivePacket);
            // If it's not the first packet received then check if the packet has the same sequence number as the previous received packet and add the sequence number to the byte array if it's not duplicate
            if(dupleCount>0){
                if(receiveData[0]!=sequence[dupleCount-1]){
                    sequence[dupleCount] = receiveData[0];
                    dupleCount++;
                }
                else{
                    duple=1;
                }
            }
            else{
                sequence[counter] = receiveData[0];
                dupleCount++;
            }
            // Converting the short number to 2 bytes
            byte[] ACK = new byte[2];
            ACK[0] = (byte)(ack & 0xff);
            ACK[1] = (byte)((ack >> 8) & 0xff);

            // New byte array which has as size the actual length of the packet received
            byte[] changedData = new byte[receivePacket.getLength()-3];
            receiveData = receivePacket.getData();
            ACK[0] = receiveData[0];
            for(int i = 3;i<receivePacket.getLength();i++) {
                changedData[i-3] = receiveData[i];
            }

            // If the packet is not duplicate
            if(duple==0) {
                fos.write(changedData);    // Write the byte array to the file
            }

            // Send the packet which contains the acknowledgement
            DatagramPacket ACKPacket = new DatagramPacket(ACK, 2, receivePacket.getAddress(), receivePacket.getPort());
            receiverSocket.send(ACKPacket);

            // If the flag of the packet that was sent is 1 then break from the loop
            if(receiveData[2] == 1){
                break;
            }
            duple = 0;
            counter++;
        }
        fos.close();    // Close the file
        receiverSocket.close();    // Close the socket
    }
}
