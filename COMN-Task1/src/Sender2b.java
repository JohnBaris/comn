/**
 * Matriculation Number:s1443483
 * Name: Ioannis Baris
 */

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class Sender2b {

    private short baseNum = 1;    // The base number
    private short seqNum = 1;     // The sequence Number
    private short windowEnd = 1;  // The number at the end of the window
    private ArrayList<Short> ackArray = new ArrayList<Short>(); // ArrayList where the acknowledgements are stored

    // Synchronised methods that handle the important variables of the class
    public synchronized void setBaseNum(short base){
        this.baseNum = base;
    }   // Method for setting the base number
    public synchronized void setSeqNum(short seq){
        this.seqNum = seq;
    }       // Method for setting the sequence number
    public synchronized short getBaseNum(){
        return this.baseNum;
    }            // Method for getting the base number
    public synchronized short getSeqNum(){
        return this.seqNum;
    }              // Method for getting the sequence number
    public synchronized void setWindowEnd(short end){
        this.windowEnd = end;
    } // Method for setting the end of window
    public synchronized short getWindowEnd(){
        return this.windowEnd;
    }        // Method for getting the end of window
    public synchronized void setAck(short ack){
        this.ackArray.add(ack);
    }     // Method for adding an acknowledgement
    public synchronized ArrayList<Short> getAck(){
        return this.ackArray;
    }    // Method for getting the ack array

    public static void main(String args[]) throws Exception {

        DatagramSocket senderSocket = new DatagramSocket(5432);    // Socket initialisation
        Sender2b sendr = new Sender2b();                           // Create an instance of the Sender2b class
        int windowSize = Integer.parseInt(args[4]);                // Window size
        int retryTimeout = Integer.parseInt(args[3]);              // Timeout value
        byte sendData[] = new byte[1027];
        byte ACK[] = new byte[2];                                  // The acknowledgment byte array
        ACK[0] = 1;                                                // Initialise the first byte
        int s;
        byte flag = 0;
        FileInputStream file = new FileInputStream(new File(args[2]));
        File image = new File(args[2]);
        double fileSize = Math.ceil((image.length() / 1024) + 1);    // The number of packets to be sent
        InetAddress IPAddress = InetAddress.getByName(args[0]);
        short seqNum = 1;                               //The sequence Number
        short baseNum = 1;                              // The base number
        int currentWindow = 0;
        long startTime = System.currentTimeMillis();    // Begin counting the runtime
        int iteration = 0;    // Initialise the acknowledgement counter

        // Loop that is iterated as long as the file has not been read completely
        while ((s = file.read(sendData, 3, sendData.length - 3)) != -1) {

            iteration++;

            // Converting the short number to 2 bytes
            sendData[1] = (byte) (seqNum & 0xff);
            sendData[0] = (byte) ((seqNum >> 8) & 0xff);

            // If it's the last iteration, i.e. end of file, flag becomes 1
            if ((iteration) == fileSize) {
                flag = 1;
            }

            sendData[2] = flag;    // Flag to be sent so that the Receiver knows when to close the socket
            byte sendData2[] = new byte[s + 3];

            // New array with size equaling the number of bytes read plus 3 (sequence number and flag)
            for (int i = 0; i < sendData2.length; i++) {
                sendData2[i] = sendData[i];
            }

            // Create the packet to be sent
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData2, s + 3, IPAddress, Integer.parseInt(args[1]));
            // Create a new instance of the SenderThread class and run it in a thread
            SenderThread sender = new SenderThread(seqNum, sendr, senderSocket, sendPacket, retryTimeout, windowSize);
            Thread send = new Thread(sender);
            send.setDaemon(true);

            /* When the sequence is within the window, i.e. we haven't sent all the packets in the window
            then we start the thread for sending the packet. */
            if (seqNum < sendr.getWindowEnd() + windowSize + 1) {

                send.start();
                seqNum++;
                sendr.setSeqNum(seqNum);
                currentWindow++;

            }

            /* If all the packets in the window have been sent or the last packet has been sent then, while we haven't
            received the acknowledgement for each packet sent, listen for incoming acknowledgements.
             */
            if (currentWindow == windowSize || iteration == fileSize) {

                while (sendr.getAck().size() < seqNum-1) {

                    DatagramPacket ACKPacket = new DatagramPacket(ACK, 2);
                    senderSocket.receive(ACKPacket);
                    baseNum = (short) ((ACK[1] & 0xFF) | ACK[0] << 8);

                    // If we have not received the acknowledgement before, then add it to the list
                    if(!sendr.getAck().contains(baseNum)) {
                        sendr.setAck(baseNum);
                    }

                    // Set the actual last number of the window
                    if(sendr.getWindowEnd()<baseNum){
                        sendr.setWindowEnd(baseNum);
                    }

                }

                sendr.setBaseNum(baseNum);
                /* The currentWindow variable takes the value of the number of packets that remain in the window after
                we received acknowledgement for the sent packets. */
                currentWindow = sendr.getSeqNum() - (sendr.getWindowEnd() + 1);
            }

        }
        System.out.println("End of file");

        senderSocket.close();    // Close the socket

        System.out.println(iteration);
        long endTime = System.currentTimeMillis();                       // Stop counting runtime
        double totalTime = (double) (endTime - startTime) / 1024.000;    // Total program execution/transmission time
        double throughput = (image.length() / 1000.000) / totalTime;     // Throughput
        System.out.println(totalTime + " Total transmission time");
        System.out.println(throughput+ " Throughput");

    }
}
