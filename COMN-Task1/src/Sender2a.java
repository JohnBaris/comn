/**
 * Matriculation Number:s1443483
 * Name: Ioannis Baris
 */

import java.io.*;
import java.net.*;

public class Sender2a {

    public static void main(String args[]) throws Exception {

        DatagramSocket senderSocket = new DatagramSocket(5432);    // Socket initialisation

        int windowSize = Integer.parseInt(args[4]); // Window size
        byte sendData[] = new byte[1027];
        byte ACK[] = new byte[2];    // The acknowledgment byte array
        DatagramPacket windowPacket[] = new DatagramPacket[windowSize];
        ACK[0] = 1;    // Initialise the first byte
        int s;
        int k = 0;
        byte flag = 0;
        FileInputStream file = new FileInputStream(new File(args[2]));
        File image = new File(args[2]);
        double fileSize = Math.ceil((image.length() / 1024) + 1);    // The number of packets to be sent
        InetAddress IPAddress = InetAddress.getByName(args[0]);
        short seqNum = 1;                                           // The sequence Number
        short baseNum = 1;                                          // The base number
        int currentWindow = 0;
        int lastPacketRetrans;
        int retransmissions = 0;                                    // Number of retransmissions
        long startTime = System.currentTimeMillis();                // Begin counting the runtime
        int iteration = 0;    // Initialise the acknowledgement counter

        // Loop that is iterated as long as the file has not been read completely
        while ((s = file.read(sendData, 3, sendData.length - 3)) != -1) {
            iteration++;

            // Converting the short number to 2 bytes
            sendData[1] = (byte) (seqNum & 0xff);
            sendData[0] = (byte) ((seqNum >> 8) & 0xff);

            // If it's the last iteration, i.e. end of file, flag becomes 1
            if ((iteration) == fileSize) {
                flag = 1;
            }

            sendData[2] = flag;    // Flag to be sent so that the Receiver knows when to close the socket
            byte sendData2[] = new byte[s + 3];

            // New array with size equaling the number of bytes read plus 3 (sequence number and flag)
            for (int i = 0; i < sendData2.length; i++) {
                sendData2[i] = sendData[i];
            }

            // If the packet is within the window, then send it and set the timeout value to the socket
            if (seqNum < baseNum + windowSize) {
                DatagramPacket sendPacket =
                        new DatagramPacket(sendData2, s + 3, IPAddress, Integer.parseInt(args[1]));
                senderSocket.send(sendPacket);
                windowPacket[k] = sendPacket;       // Store the sent packets in an array
                currentWindow++;
                seqNum++;
                k++;
                senderSocket.setSoTimeout(Integer.parseInt(args[3]));
            }

            // Check that all packets in the window have been sent or that the last packet has been sent
            if(currentWindow == windowSize || iteration == fileSize) {
                lastPacketRetrans = 0;

                /* While the the acknowledgement of the last packet in the window has not been received,
                then keep receiving. */
                while (baseNum != seqNum) {

                    DatagramPacket ACKPacket = new DatagramPacket(ACK, 2);

                    // Try to receive acknowledgment
                    try {
                        senderSocket.receive(ACKPacket);
                        baseNum = (short) ((ACK[1] & 0xFF) | ACK[0] << 8);
                        baseNum++;
                        int diff = seqNum - baseNum;

                        /* Re-set the socket timeout if the acknowledgement is not the acknowledgement of
                        the last packet of the window. */
                        if (baseNum != seqNum) {
                            senderSocket.setSoTimeout(Integer.parseInt(args[3]));
                        }
                        else{
                            k=0;
                        }

                    } catch (SocketTimeoutException e) {    // Timeout

                        int diff = (seqNum - baseNum);
                        //  Reorder the array so that it contains only the unacknowledged packets
                        for (int j = 0; j < diff; j++) {
                            windowPacket[j] = windowPacket[j + currentWindow - diff];
                        }
                        k = diff;
                        currentWindow = diff;

                        /* Check that the acknowledgement of the first packet in the window was not received
                        or the last packet has been sent and not all of the acknowledgements have been received */
                        if ((diff == windowSize) || (iteration == fileSize && diff>0)) {

                            /* If the acknowledgement of the second to last packet has been received then start
                            counting the retransmissions, in case the acknowledgement of the last packet is lost */
                            if(iteration == fileSize && baseNum == seqNum-1) {
                                lastPacketRetrans++;
                            }

                            if(lastPacketRetrans==10){
                                retransmissions = retransmissions - windowSize*10;
                                break;
                            }
                            k = diff;
                            currentWindow = diff;

                            // Retransmit the packets in the window
                            for (int j = 0; j < currentWindow; j++) {

                                DatagramPacket sendPacket =
                                        new DatagramPacket(windowPacket[j].getData(), windowPacket[j].getLength(), IPAddress, Integer.parseInt(args[1]));
                                senderSocket.send(sendPacket);
                                senderSocket.setSoTimeout(Integer.parseInt(args[3]));
                                retransmissions++;

                            }
                        }
                        else {
                            break;
                        }
                    }
                }

                int diff = seqNum - baseNum;
                currentWindow = diff;
            }
        }

        System.out.println("End of file");
        senderSocket.close();                                           // Close the socket
        System.out.println(iteration);
        long endTime = System.currentTimeMillis();                      // Stop counting runtime
        double totalTime = (double) (endTime - startTime) / 1024.000;   // Total program execution/transmission time
        double throughput = (image.length() / 1000.000) / totalTime;    // Throughput
        System.out.println(totalTime + " Total transmission time");
        System.out.println(throughput+ " Throughput");

    }
}


